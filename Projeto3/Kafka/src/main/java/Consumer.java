import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

class Consumer implements Runnable {
    private final Logger mLogger = LoggerFactory.getLogger(Consumer.class.getName());
    private String server;
    private String groupId;
    private String topic;
    private KafkaConsumer<String,JsonNode> mConsumer;
    public List countryArray  = Collections.synchronizedList(new ArrayList<JsonNode>());
    public List itemArray = Collections.synchronizedList(new ArrayList<JsonNode>());

    Consumer(String server, String groupId, String topic) {
        server = server;
        groupId = groupId;
        topic = topic;
        Properties props = consumerProps(server, groupId);
        mConsumer = new KafkaConsumer<>(props);

        ConsumerRebalanceListener listener = new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> arg0) {}
            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> arg0) {}
        };
        Pattern pattern = Pattern.compile("DBInfo.*");
        mConsumer.subscribe(pattern, listener);
    }

    public void run() {
        mLogger.info("Creating consumer thread");
        try {
            while (true) {
                ConsumerRecords<String, JsonNode> records = mConsumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, JsonNode> record : records) {
                    JsonNode jsonNode = record.value();
                    switch(jsonNode.get("schema").get("name").toString()){
                        case "\"item\"":
                            itemArray.add(jsonNode.get("payload"));
                            break;
                        case "\"country\"":
                            countryArray.add(jsonNode.get("payload"));
                            break;
                        default:
                            mLogger.info(jsonNode.get("schema").get("name").toString());
                            break;

                    }
                    mLogger.info(jsonNode.toString());
                }
            }
        } catch (WakeupException e) {
            mLogger.info("Received shutdown signal!");
        } finally {
            mConsumer.close();
        }

    }
    private Properties consumerProps(String server, String groupId) {
        String deserializerString = StringDeserializer.class.getName();

        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, deserializerString);
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.connect.json.JsonDeserializer");
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return properties;
    }

}
