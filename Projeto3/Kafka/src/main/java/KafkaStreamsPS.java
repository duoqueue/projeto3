import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.protocol.types.Field;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.connect.json.JsonDeserializer;
import org.apache.kafka.connect.json.JsonSerializer;
import org.apache.kafka.streams.KafkaStreams;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.time.Duration;

import org.apache.kafka.streams.kstream.TimeWindows;

public class KafkaStreamsPS {
    public static void main(String[] args) {
        List<String> topics = Arrays.asList("Purchases", "Sales");
        Deserializer<JsonNode> jsonDeserializer = new JsonDeserializer();
        Serializer<JsonNode> jsonSerializer = new JsonSerializer();

        Serde<JsonNode> jsonSerde = Serdes.serdeFrom(jsonSerializer, jsonDeserializer);

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "exercises-application");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, 4);
        props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, "exactly_once");

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, JsonNode> allContent = builder.stream(topics, Consumed.with(Serdes.String(), jsonSerde));
        //allContent.print(Printed.toSysOut());
        KStream<String, JsonNode>[] branches = allContent.branch(
                (key, value) -> value.get("topic").asText().startsWith("p"),
                (key, value) -> value.get("topic").asText().startsWith("s"),
                (key, value) -> true
        );

        //BRANCH [0] Purchases =====================================================================================================================================================================

        KTable<String, Double> purchaseExpenses = branches[0].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble()).
                groupByKey(Grouped.with(Serdes.String(), Serdes.Double())).
                reduce((oldVal, newVal) -> oldVal + newVal);
        purchaseExpenses.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{\"schema\":{\"type\":\"struct\",\"fields\":[{\"type\":\"int64\",\"optional\":false,\"field\":\"id\"},{\"type\":\"double\",\"optional\":true,\"field\":\"expenses\"}]},\"payload\":" + "{\"id\":" + k + ",\"expenses\":" + v + "}" + "}";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().to("item", Produced.with(Serdes.String(), jsonSerde));

        //Total Purchases

        KTable<String, Double> TotalExpenses = branches[0].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble())
                .selectKey((key, value) -> "1")
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Double()))
                .reduce((oldVal, newVal) -> oldVal + newVal);
        TotalExpenses.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{ \"schema\": { \"type\": \"struct\", \"fields\": [ { \"type\": \"int64\", \"optional\": false, \"field\": \"id\" }, { \"type\": \"double\", \"optional\": true, \"field\": \"total_expenses\" } ], \"optional\": false, \"name\": \"statistics\" }, \"payload\": { \"id\":" + 1 + ", \"total_expenses\":" + v + " } }";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().to("statistics", Produced.with(Serdes.String(), jsonSerde));


        //Total Purchases  Window
        KTable<Windowed<String>, Double> TotalExpensesWindow = branches[0].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble()).selectKey((key, value) -> "1")
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Double()))
                .windowedBy(TimeWindows.of(Duration.ofMinutes(1)))
                .reduce((oldVal, newVal) -> oldVal + newVal);
        TotalExpensesWindow.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{ \"schema\": { \"type\": \"struct\", \"fields\": [ { \"type\": \"int64\", \"optional\": false, \"field\": \"id\" }, { \"type\": \"double\", \"optional\": true, \"field\": \"total_lh_expenses\" } ], \"optional\": false, \"name\": \"statistics\" }, \"payload\": { \"id\":" + 1 + ", \"total_lh_expenses\":" + v + " } }";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().selectKey((k, v) -> "1").to("statistics", Produced.with(Serdes.String(), jsonSerde));


        //BRANCH [1] Sales =====================================================================================================================================================================

        KTable<String, Double> salesRevenues = branches[1].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble()).
                groupByKey(Grouped.with(Serdes.String(), Serdes.Double())).
                reduce((oldVal, newVal) -> oldVal + newVal);
        salesRevenues.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{\"schema\":{\"type\":\"struct\",\"fields\":[{\"type\":\"int64\",\"optional\":false,\"field\":\"id\"},{\"type\":\"double\",\"optional\":true,\"field\":\"revenue\"}]},\"payload\":" + "{\"id\":" + k + ",\"revenue\":" + v + "}" + "}";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().to("item", Produced.with(Serdes.String(), jsonSerde));

        //Total Sales

        KTable<String, Double> totalRevenue = branches[1].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble())
                .selectKey((key, value) -> "1")
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Double()))
                .reduce((oldVal, newVal) -> oldVal + newVal);

        totalRevenue.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{ \"schema\": { \"type\": \"struct\", \"fields\": [ { \"type\": \"int64\", \"optional\": false, \"field\": \"id\" }, { \"type\": \"double\", \"optional\": true, \"field\": \"total_revenues\" } ], \"optional\": false, \"name\": \"statistics\" }, \"payload\": { \"id\":" + 1 + ", \"total_revenues\":" + v + " } }";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().to("statistics", Produced.with(Serdes.String(), jsonSerde));


        //Total Sales Window

        KTable<Windowed<String>, Double> totalSales = branches[1].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble()).selectKey((key, value) -> "1")
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Double()))
                .windowedBy(TimeWindows.of(Duration.ofMinutes(1)))
                .reduce((oldVal, newVal) -> oldVal + newVal);
        totalSales.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{ \"schema\": { \"type\": \"struct\", \"fields\": [ { \"type\": \"int64\", \"optional\": false, \"field\": \"id\" }, { \"type\": \"double\", \"optional\": true, \"field\": \"total_lh_revenue\" } ], \"optional\": false, \"name\": \"statistics\" }, \"payload\": { \"id\":" + 1 + ", \"total_lh_revenue\":" + v + " } }";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().selectKey((k, v) -> "1").to("statistics", Produced.with(Serdes.String(), jsonSerde));


        //Profit  ===================================================================================================================================================================================
        KTable<String, Double> purchaseTable = branches[0].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble()).
                groupByKey(Grouped.with(Serdes.String(), Serdes.Double())).
                reduce((oldVal, newVal) -> oldVal + newVal);

        KTable<String, Double> salesTable = branches[1].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble()).
                groupByKey(Grouped.with(Serdes.String(), Serdes.Double())).
                reduce((oldVal, newVal) -> oldVal + newVal);

        KTable<String, Double> totalExpensesTable = branches[0].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble())
                .selectKey((key, value) -> "1")
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Double()))
                .reduce((oldVal, newVal) -> oldVal + newVal);

        KTable<String, Double> totalSalesTable = branches[1].mapValues(value -> value.get("nUnits").asInt() * value.get("price").asDouble())
                .selectKey((key, value) -> "1")
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Double()))
                .reduce((oldVal, newVal) -> oldVal + newVal);

        //Per item

        KTable<String, Double> profit = purchaseTable.join(salesTable, (expense, revenue) -> revenue - expense);
        profit.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{\"schema\":{\"type\":\"struct\",\"fields\":[{\"type\":\"int64\",\"optional\":false,\"field\":\"id\"},{\"type\":\"double\",\"optional\":true,\"field\":\"profit\"}]},\"payload\":" + "{\"id\":" + k + ",\"profit\":" + v + "}" + "}";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().to("item", Produced.with(Serdes.String(), jsonSerde));

        //Total

        KTable<String, Double> totalProfit = totalExpensesTable.join(totalSalesTable, (expense, revenue) -> revenue - expense);
              /*  .toStream()
                .selectKey((k,v)->"1")
                .groupByKey(Grouped.with(Serdes.String(),Serdes.Double()))
                .reduce((v1, v2) -> v1+v2);*/

        totalProfit.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{ \"schema\": { \"type\": \"struct\", \"fields\": [ { \"type\": \"int64\", \"optional\": false, \"field\": \"id\" }, { \"type\": \"double\", \"optional\": true, \"field\": \"total_profit\" } ], \"optional\": false, \"name\": \"statistics\" }, \"payload\": { \"id\":" + 1 + ", \"total_profit\":" + v + " } }";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().to("statistics", Produced.with(Serdes.String(), jsonSerde));


        //Total Window

        KTable<Windowed<String>, Double> totalProfitWindow = TotalExpensesWindow.join(totalSales, (expense, revenue) -> revenue - expense);
             /*   .toStream()
                .selectKey((k,v)->"1")
                .groupByKey(Grouped.with(Serdes.String(),Serdes.Double()))
                .windowedBy(TimeWindows.of(Duration.ofMinutes(1)))
                .reduce((oldValue, newValue)-> oldValue+newValue);*/

        totalProfitWindow.mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{ \"schema\": { \"type\": \"struct\", \"fields\": [ { \"type\": \"int64\", \"optional\": false, \"field\": \"id\" }, { \"type\": \"double\", \"optional\": true, \"field\": \"total_lh_profit\" } ], \"optional\": false, \"name\": \"statistics\" }, \"payload\": { \"id\":" + 1 + ", \"total_lh_profit\":" + v + " } }";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
                }
        ).toStream().selectKey((k, v) -> "1").to("statistics", Produced.with(Serdes.String(), jsonSerde));


        //Average  ===================================================================================================================================================================================


        KTable<String, Long> purchasePerItem = branches[0].mapValues(value -> value.get("nUnits").asLong())
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Long()))
                .reduce((v1, v2) -> v1 + v2);

        KTable<String, Long> totalPurchases = branches[0].mapValues(value -> value.get("nUnits").asLong())
                .selectKey((key, value) -> "1")
                .groupByKey(Grouped.with(Serdes.String(), Serdes.Long()))
                .reduce((v1, v2) -> v1 + v2);


        //Per Item
        KTable<String, Double> averagePerItem = purchasePerItem.join(purchaseTable, (quantidade, gasto) -> gasto / quantidade);

        averagePerItem.mapValues((k, v) -> {
            ObjectMapper obj = new ObjectMapper();
            JsonNode j = null;
            try {
                String teste = "{\"schema\":{\"type\":\"struct\",\"fields\":[{\"type\":\"int64\",\"optional\":false,\"field\":\"id\"},{\"type\":\"double\",\"optional\":true,\"field\":\"avg_spent\"}]},\"payload\":" + "{\"id\":" + k + ",\"avg_spent\":" + v + "}" + "}";
                j = obj.readTree(teste);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return j;
        }).toStream().to("item", Produced.with(Serdes.String(), jsonSerde));


        //Total

        KTable<String, Double> totalAverage = totalPurchases.join(totalExpensesTable, (quantidade, gasto) -> gasto / quantidade);
        totalAverage.mapValues((k, v) -> {
            ObjectMapper obj = new ObjectMapper();
            JsonNode j = null;
            try {
                String teste = "{ \"schema\": { \"type\": \"struct\", \"fields\": [ { \"type\": \"int64\", \"optional\": false, \"field\": \"id\" }, { \"type\": \"double\", \"optional\": true, \"field\": \"total_avg_spent\" } ], \"optional\": false, \"name\": \"statistics\" }, \"payload\": { \"id\":" + 1 + ", \"total_avg_spent\":" + v + " } }";
                j = obj.readTree(teste);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return j;
        }).toStream().filter((k, v) -> v != null).to("statistics", Produced.with(Serdes.String(), jsonSerde));

        //=======================================================================================================================

        KTable<String, Integer> country = branches[1].selectKey((key, value) -> key + value.get("country")).mapValues(value -> value.get("nUnits").asInt()).
                groupByKey(Grouped.with(Serdes.String(), Serdes.Integer())).
                reduce((oldVal, newVal) -> oldVal + newVal);

        KTable<String, JsonNode> countryJson = country.mapValues((key, value) -> {
            ObjectMapper obj = new ObjectMapper();
            JsonNode j = null;
            try {
                String teste = "{\"nSales\":" + value + ",\"country\":" + key.replaceAll("[0-9]", "") + "}";
                j = obj.readTree(teste);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return j;
        });

        countryJson.toStream().selectKey((key, value) -> key.replaceAll("[\"a-zA-Z]", "")).groupByKey(Grouped.with(Serdes.String(),jsonSerde))
                .reduce((oldValue, newValue) -> {
                    if (newValue != null && oldValue.get("nSales").asInt() >= newValue.get("nSales").asInt())
                        return oldValue;
                    else
                        return newValue;
                }).mapValues((k, v) -> {
                    ObjectMapper obj = new ObjectMapper();
                    JsonNode j = null;
                    try {
                        String teste = "{\"schema\":{\"type\":\"struct\",\"fields\":[{\"type\":\"int64\",\"optional\":false,\"field\":\"id\"},{\"type\":\"string\",\"optional\":true,\"field\":\"country\"},{\"type\":\"int32\",\"optional\":true,\"field\":\"sales\"}]},\"payload\":" + "{\"id\":" + k + ",\"country\":\"" + v.get("country").asText() + "\", \"sales\":"+ v.get("nSales").asInt() +"}}";
                        j = obj.readTree(teste);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    return j;
        }).toStream().to("item",Produced.with(Serdes.String(),jsonSerde));


        KafkaStreams streams = new KafkaStreams(builder.build(), props);
        streams.start();

        System.out.println("Reading stream from topic Purchases and Sales");
    }
}
