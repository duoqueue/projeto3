import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

class Purchase {

    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException{
        String server = "127.0.0.1:9092";
        String groupId = "purchase_app";
        String topic = "DBInfoitem";
        new Purchase(server, groupId, topic).run();
    }

    // Variables

    private final Logger mLogger = LoggerFactory.getLogger(Consumer.class.getName());
    private final String mBootstrapServer;
    private final String mGroupId;


    private final String mTopic;
    public List items  = Collections.synchronizedList(new ArrayList<JsonNode>());

    // Constructor

    Purchase(String bootstrapServer, String groupId, String topic) {
        mBootstrapServer = bootstrapServer;
        mGroupId = groupId;
        mTopic = topic;
    }

    // Public

    void run() throws ExecutionException, InterruptedException, JsonParseException, IOException {
        mLogger.info("Creating purchase thread");

        ConsumerRunnable consumerRunnable = new ConsumerRunnable(mBootstrapServer, mGroupId, mTopic);
        Thread thread = new Thread(consumerRunnable);
        thread.start();
        String server = "127.0.0.1:9092";
        String topic = "Purchases";
        Properties props = producerProps(server);
        KafkaProducer<String,JsonNode> mProducer = new KafkaProducer<>(props);
        mLogger.info("Producer initialized");
        while(true){
            if(items.size()!=0) {
                Random r = new Random();
                JsonNode jsonNode = (JsonNode) items.get(r.nextInt(items.size()));
                String id = jsonNode.get("id").asText();
                String nUnits = "2";//Integer.toString(r.nextInt(25)+1);
                String price = "2";//Double.toString(1+r.nextDouble()*(15));
                String json = "{\"topic\":\"purchase\",\"nUnits\":"+nUnits+",\"price\":"+price+"}";
                ObjectMapper mapper = new ObjectMapper();
                JsonNode value = mapper.readTree(json);
                ProducerRecord<String,JsonNode> record = new ProducerRecord<String,JsonNode>(topic,id,value);
                mLogger.info("Sending "+id+" "+json);
                mProducer.send(record,(recordMetadata, e) -> {
                    if (e != null) {
                        mLogger.error("Error while producing", e);
                        return;
                    }
                    mLogger.info("Received new meta. Topic: " + recordMetadata.topic()
                            + "; Partition: " + recordMetadata.partition()
                            + "; Offset: " + recordMetadata.offset()
                            + "; Timestamp: " + recordMetadata.timestamp());
                }).get();
            }
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Properties producerProps(String bootstrapServer){
        String serializerString = StringSerializer.class.getName();
        Properties props= new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, serializerString);
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.connect.json.JsonSerializer");

        return props;
    }

    // Inner classes

    private class ConsumerRunnable implements Runnable {
        private KafkaConsumer<String, JsonNode> mConsumer;

        ConsumerRunnable(String bootstrapServer, String groupId, String topic) {

            Properties props = consumerProps(bootstrapServer, groupId);
            mConsumer = new KafkaConsumer<>(props);
            mConsumer.subscribe(Collections.singletonList(topic));
        }

        @Override
        public void run() {
            try {
                while (true) {
                    ConsumerRecords<String, JsonNode> records = mConsumer.poll(Duration.ofMillis(100));

                    for (ConsumerRecord<String,JsonNode> record : records) {
                        JsonNode jsonNode = record.value();
                        mLogger.info(jsonNode.get("payload").toString());
                        mLogger.info(record.key());
                        if(!items.contains(jsonNode.get("payload")))
                            items.add(jsonNode.get("payload"));
                    }
                }
            } catch (WakeupException e) {
                mLogger.info("Received shutdown signal!");
            } finally {
                mConsumer.close();
            }
        }

        void shutdown() {
            mConsumer.wakeup();
        }

        private Properties consumerProps(String bootstrapServer, String groupId) {
            String deserializerString = StringDeserializer.class.getName();

            Properties properties = new Properties();
            properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
            properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
            properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, deserializerString);
            properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.connect.json.JsonDeserializer");
            properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

            return properties;
        }
    }
}

