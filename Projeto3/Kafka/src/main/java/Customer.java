import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.Random;

public class Customer {

    public static void main(String[] args) throws InterruptedException, JsonProcessingException {
        String server = "127.0.0.1:9092";
        String groupId = "customer";
        String consumeTopic = "DBInfo";
        String produceTopic = "Sales";


        Properties props = producerProps(server);
        KafkaProducer<String, JsonNode> mProducer = new KafkaProducer<>(props);

        Consumer customerConsumer = new Consumer(server, groupId, consumeTopic);
        Thread consumerThread = new Thread(customerConsumer);
        consumerThread.start();
        Random rand = new Random(System.currentTimeMillis());
        while (true) {
            Thread.sleep(4000);
            if (customerConsumer.countryArray.size() != 0 && customerConsumer.itemArray.size() != 0) {
                System.out.println(customerConsumer.countryArray);
                System.out.println(customerConsumer.itemArray);

                //Produce
                JsonNode randomCountry = (JsonNode) customerConsumer.countryArray.get(rand.nextInt(customerConsumer.countryArray.size()));
                JsonNode randomItem = (JsonNode) customerConsumer.itemArray.get(rand.nextInt(customerConsumer.itemArray.size()));
                int nUnits = 2;//rand.nextInt(25) + 1;
                Double price = 2.0;//rand.nextDouble() * (15) + 1;
                String json = "{\"topic\":\"sales\",\"name\":"+randomItem.get("name").toString()+",\"nUnits\":" + nUnits + ",\"price\":" + price + ",\"country\":" + randomCountry.get("name").toString() + "}";

                ObjectMapper mapper = new ObjectMapper();
                JsonNode value = mapper.readTree(json);

                ProducerRecord<String, JsonNode> record = new ProducerRecord<String, JsonNode>(produceTopic, randomItem.get("id").asText(), value);
                mProducer.send(record);
            }
        }
    }

    private static Properties producerProps(String server) {
        String serializerString = StringSerializer.class.getName();
        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, serializerString);
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.connect.json.JsonSerializer");

        return props;
    }
}
