import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.*;

@Path("/api")
public class RestAPI {
    Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/projeto3?serverTimezone=UTC", "root","ttye45+9" );

    public RestAPI() throws SQLException {
    }

    @Path("/addCountry")
    @GET
    public String addCountry(@QueryParam("name") String name) {
        String query= "Insert into Country (name) values (?) ";
        try {
            PreparedStatement preStmt = con.prepareStatement(query);
            preStmt.setString(1,name);
            preStmt.execute();
            return "Sucess!";
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }

    @Path("/addItem")
    @GET
    public String addItem(@QueryParam("name") String name) {
        String query= "Insert into item (name) values (?) ";
        try {
            PreparedStatement preStmt = con.prepareStatement(query);
            preStmt.setString(1,name);
            preStmt.execute();
            return "Sucess!";
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }

    @Path("/getCountries")
    @GET
    public String getCountries() {
        String query= "Select name from country ";
        try {
            Statement stmt= con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String resultString="";
            while(rs.next()){
                resultString+=rs.getString("name")+"\n";

            }
            return resultString;
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }

    @Path("/getItems")
    @GET
    public String getItems() {
        String query= "Select name from item ";
        try {
            Statement stmt= con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String resultString="";
            while(rs.next()){
                resultString+="Name: "+rs.getString("name")+"\n";
            }
            return resultString;
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }

    @Path("/getItem")
    @GET
    public String getItem(@QueryParam("name") String name) {
        String query= "Select revenue,expenses,profit,avg_spent,country,sales from item where name=?";
        try {
            PreparedStatement preStmt = con.prepareStatement(query);
            preStmt.setString(1,name);
            ResultSet rs = preStmt.executeQuery();
            String resultString="";
            while(rs.next()){
                resultString+="Revenue: " + rs.getString("revenue")+"\n";
                resultString+="Expenses: "+rs.getString("expenses")+"\n";
                resultString+="Profit: " + rs.getString("profit")+"\n";
                resultString+="Average Spent: "+ rs.getString("avg_spent")+"\n";
                resultString+="Best Country: "+ rs.getString("country")+" With: "+rs.getInt("sales")+" sales\n";
            }
            return resultString;
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }

    @Path("/getStatistics")
    @GET
    public String getStatistics(@QueryParam("name") String name) {
        String query= "Select total_revenues,total_expenses,total_profit,total_avg_spent from statistics ";
        try {
            Statement stmt= con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String resultString="";
            while(rs.next()){
                resultString+="Total Revenue: " + rs.getString("total_revenues")+"\n";
                resultString+="Total Expenses: "+rs.getString("total_expenses")+"\n";
                resultString+="Total Profit: " + rs.getString("total_profit")+"\n";
                resultString+="Total Average Spent: "+ rs.getString("total_avg_spent")+"\n";
            }
            return resultString;
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }

    @Path("/getHighestProfit")
    @GET
    public String getHighestProfit(@QueryParam("name") String name) {
        String query= "Select id, name from item ORDER by profit DESC limit 1 ";
        try {
            Statement stmt= con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String resultString="";
            while(rs.next()){
                resultString+="Id: " + rs.getString("id")+ " Name: "+rs.getString("name")+"\n";
            }
            return resultString;
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }

    @Path("/getHourStatistics")
    @GET
    public String getHourStatistics(@QueryParam("name") String name) {
        String query= "Select total_lh_revenue,total_lh_expenses,total_lh_profit from statistics ";
        try {
            Statement stmt= con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            String resultString="";
            while(rs.next()){
                resultString+="Total Revenue: " + rs.getString("total_lh_revenue")+"\n";
                resultString+="Total Expenses: "+rs.getString("total_lh_expenses")+"\n";
                resultString+="Total Profit: " + rs.getString("total_lh_profit")+"\n";
            }
            return resultString;
        } catch (SQLException e) {
            e.printStackTrace();
            return "Failed!";
        }
    }


}
