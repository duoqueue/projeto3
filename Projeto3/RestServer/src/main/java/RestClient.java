import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import java.util.Scanner;

public class RestClient {
    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        String uri = "http://localhost:9998/api";
        Scanner scan = new Scanner(System.in);
        String input = "";
        String newUri = uri;
        WebTarget webTarget;
        Invocation.Builder invocationBuilder;
        String response;
        while (!(input = scan.nextLine()).equals("exit")) {
            switch (input) {
                case "addCountry":
                    System.out.println("Country Name:");
                    String country=scan.nextLine();
                    newUri = uri + "/addCountry?name="+country;
                    webTarget = client.target(newUri);
                    invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                    response = invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                case "addItem":
                    System.out.println("Item Name:");
                    String itemName=scan.nextLine();
                    newUri = uri + "/addItem?name="+itemName;
                    webTarget = client.target(newUri);
                    invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                    response = invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                case "getCountries":
                    newUri = uri + "/getCountries";
                    webTarget = client.target(newUri);
                    invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                    response = invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                case "getItems":
                    newUri= uri+ "/getItems";
                    webTarget=client.target(newUri);
                    invocationBuilder= webTarget.request(MediaType.APPLICATION_JSON);
                    response= invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                case "getItem":
                    System.out.println("Item Name:");
                    String item=scan.nextLine();
                    newUri = uri + "/getItem?name="+item;
                    webTarget = client.target(newUri);
                    invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                    response = invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                case "getStatistics":
                    newUri = uri + "/getStatistics";
                    webTarget = client.target(newUri);
                    invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                    response = invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                case "getHighProfit":
                    newUri = uri + "/getHighestProfit";
                    webTarget = client.target(newUri);
                    invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                    response = invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                case "getHourStatistics":
                    newUri = uri + "/getHourStatistics";
                    webTarget = client.target(newUri);
                    invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
                    response = invocationBuilder.get(String.class);
                    System.out.println(response);
                    break;
                default:
                    System.out.println("Unknown Command!");
            }
        }
    }
}
